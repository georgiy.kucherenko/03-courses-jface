package com.luxoft.courses_api.service;

import com.luxoft.courses_api.course_managers.*;
import com.luxoft.courses_api.exceptions.*;
import com.luxoft.courses_api.models.*;

/**
 * Service class operating with CourseManager. 
 * Goals of this class are:
 * <p><ul>
 * <li>load Course objects from some source</li>
 * <li>save Course objects to some source</li>
 * <li>create blank Course object</li>
 * </ul>
 * @author Georgiy Kucherenko
 *
 */
public class SchoolService {
	private final CourseManager courseManager;
	
	/**
	 * AllArgs public constructor 
	 * 
	 * @param courseManager - object of class that implements CourseManager
	 */
	public SchoolService(CourseManager courseManager) {
		this.courseManager = courseManager;
	}

	/**
	 * Public method that loads Course object from some source
	 * 
	 * @param path - source path (for example - full path to file)
	 * @return - Course object
	 * @throws Exception - in case of problems with deserialization, 
	 * or problems with source (for example with file) etc.
	 */
	public Course loadCourses(String path) throws CourseManagerException {
		return courseManager.loadCourse(path);
	}

	/**
	 * Public method. Saves Course object to the specified path (file path for example)
	 * @param path - source path (for example - full path to file) 
	 * @param course - Course object to be saved
	 * @return - true if saved successfully. In other case - returns false
	 * @throws Exception - in case of troubles with source (for example with specified file path)
	 */
	public void saveCourse(String path, Course course) throws CourseManagerException {
		courseManager.saveCourse(path, course);
	}
	
	/**
	 * Public method that provides blank Course object. 
	 * 
	 * @return - Course object
	 */
	public Course createCourse(){
		return courseManager.createCourse();
	}
}
