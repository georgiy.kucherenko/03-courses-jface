package com.luxoft.courses_api.course_managers;

import java.io.*;

import com.luxoft.courses_api.exceptions.FileCourseManagerException;
import com.luxoft.courses_api.models.Course;

/**
 * Implementation of CourseManager interface. 
 * Dedicated to operating with Files as sources
 * 
 * @author Georgiy Kucherenko
 *
 */
public class FileCourseManager implements CourseManager {

	private static final String TRY_TO_SAVE_NULL = "Null cannot be saved to file";

	@Override
	public Course loadCourse(String path) throws FileCourseManagerException{
		try (ObjectInputStream oInputStream = new ObjectInputStream(new FileInputStream(path))) {
			return (Course) oInputStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			throw new FileCourseManagerException(ex.getMessage(), ex);
		} 
	}

	@Override
	public void saveCourse(String path, Course course) throws FileCourseManagerException{
		if (course == null) {
			throw new FileCourseManagerException(TRY_TO_SAVE_NULL);
		}
		try (ObjectOutputStream oOutputStream = new ObjectOutputStream(new FileOutputStream(path))){
			oOutputStream.writeObject(course);
		} catch (IOException ex) {
			throw new FileCourseManagerException(ex.getMessage(), ex);
		}
	}
}
