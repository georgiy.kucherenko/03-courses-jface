package com.luxoft.courses_api.course_managers;

import java.util.ArrayList;

import com.luxoft.courses_api.exceptions.*;
import com.luxoft.courses_api.models.*;

/**
 * Abstraction for an object that can manage Course object. 
 * Basically it can Load, Save and Create Course objects. 
 * 
 * @author Georgiy Kucherenko
 *
 */
public interface CourseManager {

	/**
	 * Abstract method for loading course from some Source.  
	 * 
	 * @param path - source path (for example: path to file)
	 * @return - Course entity
	 * @throws CourseManagerException in case of problems with path or with source. 
	 * Message with detailed information should be provided together with Exception
	 */
	Course loadCourse(String path) throws CourseManagerException;
	
	/**
	 * Abstract method for saving Course to some destination
	 * 
	 * @param path - source path (for example: path to file)
	 * @param course - Course entity
	 * @return - true if saved successfully. In other case returns false
	 * @throws CourseManagerException in case of problems with path or with source. 
	 */
	void saveCourse(String path, Course course) throws CourseManagerException;

	/**
	 * Creates and provides a new Course object with empty fields.
	 * 
	 * @return - new Course object with initialized fields
	 */
	default Course createCourse() {
		return new Course(new ArrayList<Report>());
	}
}
