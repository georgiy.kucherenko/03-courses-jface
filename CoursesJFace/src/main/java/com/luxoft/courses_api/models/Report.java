package com.luxoft.courses_api.models;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class holds information about Student's academic performance.
 * Class implements Serializable interface. 
 * It is marked as final in order to avoid problems with serializations in case of inheritance
 * 
 * @author Georgiy Kucherenko
 *
 */
public final class Report implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String group;
	private boolean done;
	
	/** 
	 * AllArgs public constructor 
	 * 
	 * @param name - Student's name
	 * @param group - Student's group
	 * @param done - shows did the student make SWT Task
	 */
	public Report(String name, String group, Boolean done) {
		this.name = name;
		this.group = group;
		this.done = done;
	}

	/**
	 * Getter 
	 * 
	 * @return Student's name
	 */
	public String getName() {
		return name;
	}

	/** 
	 * Setter
	 * 
	 * @param name - Student's name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter
	 * 
	 * @return Student's group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Setter 
	 * 
	 * @param group Student's group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Getter
	 * 
	 * @return true if Student made SWT Task. In other case returns false
	 */
	public boolean getDone() {
		return done;
	}

	/**
	 * Setter
	 * 
	 * @param done - sets true if Student made SWT task. In other case sets false
	 */
	public void setDone(boolean done) {
		this.done = done;
	}
	
	@Override
	public String toString() {
		return "Report [name=" + name + ", group=" + group + ", done=" + done + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(done, group, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Report other = (Report) obj;
		return done == other.done && Objects.equals(group, other.group) && Objects.equals(name, other.name);
	}
	
	@Override
	public Report clone() {
		try {
			return (Report) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
	}

	public void setValues(Report report) {
		this.name = report.getName();
		this.group = report.getGroup();
		this.done = report.getDone();
	}
}
