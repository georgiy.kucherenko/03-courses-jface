package com.luxoft.courses_api.models;

import java.io.Serializable;
import java.util.*;

/**\
 * This class contains information about students' academic performance. 
 * It contains a list of Reports. Each report represents information about Student 
 * and results of Examine of this Student. 
 * Also class provides tools for modifying this list
 * 
 * @author Georgiy Kucherenko
 *
 */
public final class Course implements Serializable{
	private static final long serialVersionUID = 1L;
	private List<Report> reportsList;
	

	/**
	 * Public constructor
	 * 
	 * @param reportsList - list of Report to operate with
	 */
	public Course(List<Report> reportsList) {
		this.reportsList = reportsList;
	}

	/**
	 * Setter 
	 * 
	 * @param report - List of Reports
	 */
	public void setReportList(List<Report> report) {
		this.reportsList = report;
	}
	
	/**
	 * Getter 
	 * 
	 * @return List of Reports
	 */
	public List<Report> getReportList() {
		return reportsList;
	}
	
	/**
	 * Method for adding a report to Reports' list field
	 * 
	 * @param report - single Report
	 */
	public void addReport(Report report) {
		reportsList.add(report);
	}
	
	/**
	 * Method for setting Report to reportList by index.
	 * It replaces previous value in the list
	 * 
	 * @param index - index of reportList
	 * @param report - value to be inserted
	 */
	public void setReport(int index, Report report) {
		reportsList.set(index, report);
	}
	
	/**
	 * Method for removing a report from Report List field
	 * 
	 * @param report - to be removed
	 * @return true if removed successfully. In other case returns false
	 */
	public boolean removeReport(Report report) {
		return reportsList.remove(report);
	}

	@Override
	public String toString() {
		return "Course [report=" + reportsList + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(reportsList);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		return Objects.equals(reportsList, other.reportsList);
	}

}
