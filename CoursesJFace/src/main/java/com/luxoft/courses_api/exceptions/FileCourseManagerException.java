package com.luxoft.courses_api.exceptions;

/**
 * This exception should be thrown by FileCourseManager.
 * FileCourseManager can catch IOException or ClassNotFoundException during serialization or deserialization. 
 * And in this case FileCourseManagerException is thrown with the message from caught exception and cause (reason). 
 * (this exceptions is not going to be serialized that's why SuppressWarnings annotation is applied) 
 * 
 * @author Georgiy Kucherenko
 *
 */
@SuppressWarnings("serial")
public class FileCourseManagerException extends CourseManagerException {

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 * @param cause - the reason 
	 */
	public FileCourseManagerException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 */
	public FileCourseManagerException(String message) {
		super(message);
	}
	
}

