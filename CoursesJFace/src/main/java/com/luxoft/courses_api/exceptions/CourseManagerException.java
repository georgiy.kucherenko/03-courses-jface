package com.luxoft.courses_api.exceptions;

/**
 * This exception should be thrown by CourseManager.
 * It is thrown when CourseManager faces problems with loading or saving data from/to some source 
 * (this exceptions is not going to be serialized that's why SuppressWarnings annotation is applied) 
 * 
 * @author Georgiy Kucherenko
 *
 */
@SuppressWarnings("serial")
public class CourseManagerException extends Exception {

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 * @param cause - the reason 
	 */
	public CourseManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Public constructor
	 * 
	 * @param message - message of exception
	 */
	public CourseManagerException(String message) {
		super(message);
	}
	
	
}

