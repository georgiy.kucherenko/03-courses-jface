package com.luxoft.gui_jface.gui.viewer_sorter;

import java.util.Comparator;
import java.util.regex.Pattern;

/**
 * Instance of this class is a comparator for using in TableViewer.
 * This comparator compares String. Main difference with standard
 * comparator for Strings is that Letters go always before digits.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class LetterFirstComparator implements Comparator<String> {
	private static final Pattern pattern = Pattern.compile("\\d");

	@Override
	public int compare(String stringA, String stringB) {
		String[] letterArrayA = stringA.toUpperCase().split("");
		String[] letterArrayB = stringB.toUpperCase().split("");
		int minValue = Math.min(stringA.length(), stringB.length());

		for (int k = 0; k < minValue; k++) {
			if (!letterArrayA[k].equals(letterArrayB[k])) {
				boolean aIsDigit = pattern.matcher(letterArrayA[k]).find();
				boolean bIsDigit = pattern.matcher(letterArrayB[k]).find();
				
				if ((!aIsDigit && !bIsDigit) || (aIsDigit && bIsDigit)) {
					return letterArrayA[k].compareTo(letterArrayB[k]);
				} else {
					return aIsDigit ? 1 : -1;
				}
			}
		}
		return letterArrayA.length - letterArrayB.length;
	}
}