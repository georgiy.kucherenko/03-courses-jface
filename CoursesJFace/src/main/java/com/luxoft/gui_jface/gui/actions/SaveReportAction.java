package com.luxoft.gui_jface.gui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.util.ActionStatusUpdater;
import com.luxoft.gui_jface.gui.listeners.*;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;
/**
 * This action saves changes to a Report object. 
 * In order to do it this action does following: 
 * <p><ul>
 * <li>calls notify method of appropriate GUI's button with new event as a parameter</li>
 * </ul>
 * 
 * @author Georgiy Kucherenko
 *
 */
public class SaveReportAction extends Action implements Listener, SubMenuTuner{
	private final CoursesMain coursesMain;

	/**
	 * Public constructor
	 * <p>Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 */
	public SaveReportAction(CoursesMain coursesMain) {
		this.coursesMain = coursesMain;
		setText(SAVE_REPORT);
		setAccelerator(SWT.CTRL + SWT.ALT + 'S');
		setEnabled(false);
	}

	@Override
	public void run() {
		coursesMain.getReportEditor().notifySaveButtonListener(SWT.Selection, new Event());
	}

	@Override
	public void handleEvent(Event event) {
		ActionStatusUpdater.updateEnableStatus(this, event, ENABLE_SAVE_BUTTON, DISABLE_SAVE_BUTTON);
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addEnableSettingListenerToSaveButton(this);
	}
}
