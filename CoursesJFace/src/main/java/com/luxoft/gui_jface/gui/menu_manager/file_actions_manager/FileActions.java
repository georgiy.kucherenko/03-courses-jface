package com.luxoft.gui_jface.gui.menu_manager.file_actions_manager;

/**
 * This class holds all possible types of operations from File menu of Courses app 
 * 
 * @author Georgiy Kucherenko
 *
 */
public enum FileActions {
	NEW, 
	OPEN,
	SAVE,
	CLOSE;
	
	/**
	 * Checks if concrete enum equals given enum.
	 * Given enum is represented by it's stringValue
	 * 
	 * @param string - string representation of enum
	 * @return - if enum equals given enum (represented by string value
	 */
	public boolean isNew(String string) {
		return NEW.name().equals(string);
	}

	/**
	 * Checks if concrete enum equals given enum.
	 * Given enum is represented by it's stringValue
	 * 
	 * @param string - string representation of enum
	 * @return - if enum equals given enum (represented by string value
	 */
	public boolean isOpen(String string) {
		return OPEN.name().equals(string);
	}
	/**
	 * Checks if concrete enum equals given enum.
	 * Given enum is represented by it's stringValue
	 * 
	 * @param string - string representation of enum
	 * @return - if enum equals given enum (represented by string value
	 */	
	public static boolean isSave(String string) {
		return SAVE.name().equals(string);
	}
	/**
	 * Checks if concrete enum equals given enum.
	 * Given enum is represented by it's stringValue
	 * 
	 * @param string - string representation of enum
	 * @return - if enum equals given enum (represented by string value
	 */	
	public boolean isClose(String string) {
		return CLOSE.name().equals(string);
	}
}
