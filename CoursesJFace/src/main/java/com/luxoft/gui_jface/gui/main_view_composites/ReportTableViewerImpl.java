package com.luxoft.gui_jface.gui.main_view_composites;

import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.VIEWER_EMPTY;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.VIEWER_NOT_EMPTY;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.VIEWER_SELECTION_CHANGED;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.luxoft.courses_api.models.Report;
import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.cell_modifiers.ReportCellModifier;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;
import com.luxoft.gui_jface.gui.util.Procedure;
import com.luxoft.gui_jface.gui.viewer_sorter.LetterFirstComparator;
import com.luxoft.gui_jface.gui.viewer_sorter.TableViewerSorter;
import com.luxoft.gui_jface.gui.widget_providers.TableViewerContentProvider;
import com.luxoft.gui_jface.gui.widget_providers.TableViewerLabelProvider;

/**
 * Instance of this class is an integral part of CoursesMain object. It extends
 * TableViewer class. And represents content of left part of user interface
 * (CoursesMain class). 
 * 
 * @author Georgiy Kucherenko
 */
public class ReportTableViewerImpl extends TableViewer implements ReportTableViewer {
	public static final String NAME = "Name";
	public static final String GROUP = "Group";
	public static final String DONE = "Done";
	public static final String[] PROPS = { NAME, GROUP, DONE };
	private Procedure notifier;


	/**
	 * Public constructor
	 * 
	 * @param parent      - instance of Composite class from CoursesMain object
	 * @param style       - SWT style bits
	 * @param coursesMain - instance of CoursesMain
	 */
	public ReportTableViewerImpl(Composite parent, int style, CoursesMain coursesMain) {
		super(parent, style);

		createEditorNotifier(coursesMain);
		setupViewer(coursesMain);
		addSelectionChangedListener(createTableViewerListener(coursesMain));
	}
	
	@Override
	public void setSelectionOfTable(TableItem tableItem) {
		getTable().setSelection(tableItem);
	}

	@Override
	public void remove(TableItem tableItem) {
		super.remove(tableItem);
	}

	@Override
	public void addReport(Report report) {
		super.add(report);
	}

	@Override
	public void setInput(List<Report> reportList) {
		super.setInput(reportList);
	}

	@Override
	public void setSelectionToNull() {
		setSelection(null);
	}

	@Override
	public TableItem[] getItemsOfTable() {
		return super.getTable().getItems();
	}
	
	@Override
	public TableItem[] getSelectionOfTable() {
		return getTable().getSelection();
	}
	
	@Override
	public void refresh() { 
		super.refresh();
		notifier.perform();
	}
	
	private void createEditorNotifier(CoursesMain coursesMain) {
		notifier = createTableEditorNotifier(coursesMain);
		
	}

	private void setupViewer(CoursesMain coursesMain) {
		setContentProvider(new TableViewerContentProvider());
		setLabelProvider(new TableViewerLabelProvider());
		setupTable();
		setColumnProperties(PROPS);
		setCellModifier(new ReportCellModifier(createUpdater(coursesMain)));
		setCellEditors(createCellEditors());
		setComparator(new TableViewerSorter(new LetterFirstComparator()));
	}
	
	private Procedure createUpdater(CoursesMain coursesMain) {
		return () -> {
			refresh();
			coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(VIEWER_SELECTION_CHANGED));
		};
	}

	private Procedure createTableEditorNotifier(CoursesMain coursesMain) {
		return () -> {
			if (coursesMain.getCourse()!=null) {
				if (coursesMain.getCourse().getReportList().isEmpty()) {
					coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(VIEWER_EMPTY));
				} else {
					coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(VIEWER_NOT_EMPTY));
				}
			}
		};
	}
	
	private void setupTable() {
		Table table = getTable();
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		createTableColumn(NAME, 80);
		createTableColumn(GROUP, 60);
		createTableColumn(DONE, 70);
		
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}
	
	private void createTableColumn(String columnName, int columnWidth) {
		TableColumn tc = new TableColumn(getTable(), SWT.LEFT);
		tc.setWidth(columnWidth);
		tc.setText(columnName);
		tc.addSelectionListener(SelectionListener.widgetDefaultSelectedAdapter(this::sortAndRefresh));
	}

	private void sortAndRefresh(SelectionEvent e) {
		TableColumn thisColumn = (TableColumn) e.widget;
		
		List<String> columns = Arrays.asList(thisColumn.getParent().getColumns()).stream()
				.map(a -> a.getText())
				.collect(Collectors.toList());
		((TableViewerSorter) getComparator()).doSort(columns.indexOf(thisColumn.getText()));
		
		refresh();
	}
	
	private CellEditor[] createCellEditors() {
		return new CellEditor[] { 
				new TextCellEditor(getTable()),
				new TextCellEditor(getTable()), 
				new CheckboxCellEditor(getTable()) 
				};
	}

	private ISelectionChangedListener createTableViewerListener(CoursesMain coursesMain) {
		return event -> {
				Report report = (Report) ((IStructuredSelection) event.getSelection()).getFirstElement();

				if (report != null) {
					ReportEditor reportEditor = coursesMain.getReportEditor();
					reportEditor.notifyReportEditor(new ReportEditorEvent(VIEWER_SELECTION_CHANGED));
				}
		};
	}
}
