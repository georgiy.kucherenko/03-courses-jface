package com.luxoft.gui_jface.gui.main_view_composites;

import java.util.List;

import org.eclipse.swt.widgets.TableItem;

import com.luxoft.courses_api.models.Report;

/**
 * Object of this interface is manipulating the data from table part 
 * of CoursesMain App and manipulating the selection in table also.
 * 
 * @author Georgiy Kucherenko
 */
public interface ReportTableViewer {

	/**
	 * Method removes TableItem from tableViewer
	 *  
	 * @param tableItem - item to be removed from viewer
	 */
	void remove(TableItem tableItem);

	/**
	 * Refreshes the table and notifies about refresh if it is needed
	 */
	void refresh();

	/**
	 * Returns an array of table's item that User has selected
	 * 
	 * @return selected items
	 */
	TableItem[] getSelectionOfTable();

	/**
	 * Adds report to TableViewer
	 * 
	 * @param report - report to be added
	 */
	void addReport(Report report);

	/**
	 * Sets selection of table to given tableItem
	 * 
	 * @param tableItem - item to be selected
	 */
	void setSelectionOfTable(TableItem tableItem);

	/**
	 * This method resets the selection. After performing - nothing is selected
	 */
	void setSelectionToNull();

	/**
	 * Sets source for table. 
	 * 
	 * @param reportList - source object
	 */
	void setInput(List<Report> reportList);

	/**
	 * Returns all TableItems of ReportTableViewer
	 * 
	 * @return - all items of ReportTableViewer
	 */
	TableItem[] getItemsOfTable();
}
