package com.luxoft.gui_jface.gui.models;

import java.util.List;

import org.eclipse.swt.widgets.Event;

import com.luxoft.courses_api.models.Report;

/**
 * Class extends org.eclipse.swt.widgets.Event.
 * Events of this class are used for notifying objects in Courses App.
 * Object of this class always contains objects of ReportEditorActionType.
 * These objects are markers for performing. Event contains 
 * 1 ReportEditorActionType as minimal.
 * Also objects of this class can contain Report optionally. 
 * 
 * @author Georgiy Kucherenko
 *
 */
public class ReportEditorEvent extends Event {
	private List<ReportEditorActionType> actionTypes;
	private Report report;
	
	/**
	 * Public constructor
	 * 
	 * @param actionTypes - list of actions to be performed
	 * @param report - usually this report is set to widgets in UI
	 */
	public ReportEditorEvent(List<ReportEditorActionType> actionTypes, Report report) {
		this.actionTypes = actionTypes;
		this.report = report;
	}
	
	/**
	 * Public constructor
	 * 
	 * @param actionType - action to be performed
	 * @param report - usually this report is set to widgets in UI
	 */	
	public ReportEditorEvent(ReportEditorActionType actionType, Report report) {
		this(List.of(actionType), report);
	}	
	
	/**
	 * Public constructor
	 * 
	 * @param actionTypes - list of actions to be performed
	 */
	public ReportEditorEvent(List<ReportEditorActionType> actionTypes) {
		this(actionTypes, null);
	}

	/**
	 * Public constructor
	 * 
	 * @param actionTypes - action to be performed
	 */
	public ReportEditorEvent(ReportEditorActionType actionType) {
		this(actionType, null);
	}
	
	/**
	 * Getter for actionTypes
	 * 
	 * @return - list of Actions to be performed
	 */
	public List<ReportEditorActionType> getActionTypes() {
		return actionTypes;
	}

	/**
	 * Getter
	 * 
	 * @return report object
	 */
	public Report getReport() {
		return report;
	}
}
