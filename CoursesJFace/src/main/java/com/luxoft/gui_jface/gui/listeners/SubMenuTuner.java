package com.luxoft.gui_jface.gui.listeners;

/**
 * Objects that implement this interface are supposed to be tuned by request.
 *  
 * @author Georgiy Kucherenko
 *
 */
public interface SubMenuTuner {
	
	/**
	 * Method for tuning object by request. 
	 */
	void tuneListener();
}
