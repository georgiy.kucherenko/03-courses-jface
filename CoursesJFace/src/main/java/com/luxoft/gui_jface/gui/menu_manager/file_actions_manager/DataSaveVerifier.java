package com.luxoft.gui_jface.gui.menu_manager.file_actions_manager;

import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.CHECK_IF_NEED_TO_SAVE_REPORT;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.RESET_STATE;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;

/**
 * Utility class for checking weather data from Courses App should be saved or not. 
 * This is singleton class.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class DataSaveVerifier {
	private static final String DIALOG_QUESTION = "Please reply";
	private static final String DIALOG_QUESTION_MESSAGE = "Would you like to save changes in COURSE before quiting?";
	private static final DataSaveVerifier VERIFIER = new DataSaveVerifier();
	
	private DataSaveVerifier() {
	}
	
	/**
	 * Method for getting an instance of singleton
	 * 
	 * @return instance of this class
	 */
	public static DataSaveVerifier getInstance() {
		return VERIFIER;
	}


	/**
	 * Method initiates saving the currently opened report if it should be saved,
	 * and resets state of DataSynchronizer object to null
	 * 
	 * @param coursesMain - instance of CoursesMain
	 */
	public void saveReportIfNeeded(CoursesMain coursesMain) {
		coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(List.of(
				CHECK_IF_NEED_TO_SAVE_REPORT,
				RESET_STATE
			)));
	} 
	
	/**
	 * Method initiates saving the currently opened course
	 *  
	 * @param coursesMain - instance of CoursesMain
	 * @param cam - manager providing tool for saving the course object 
	 */
	public void saveCourseIfNeeded(CoursesMain coursesMain) {
		if (coursesMain.getCourse().getReportList().size() > 0
				&& askQuestion(coursesMain.getShell())) {
			
			FileActionsManager.getInstance().getAction(FileActions.SAVE).run();
		}
	}

	private boolean askQuestion(Shell shell) {
		return MessageDialog.openQuestion(shell, DIALOG_QUESTION, DIALOG_QUESTION_MESSAGE);
	}
}
