package com.luxoft.gui_jface.gui.actions;

import org.eclipse.core.runtime.*;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import com.luxoft.courses_api.exceptions.CourseManagerException;
import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.util.ActionStatusUpdater;
import com.luxoft.gui_jface.gui.listeners.*;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.*;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;

/**
 * This action saves Course object to a file. 
 * In order to do it this action does following: 
 * <p><ul>
 * <li>Accepts file's full path through FileDialog instance
 * <li>Communicates with Courses API through CoursesMain object</li>
 * </ul>
 * 
 * @author Georgiy Kucherenko
 *
 */
public class SaveFileAction extends Action  implements Listener, SubMenuTuner{
	private final CoursesMain coursesMain;
	private final DataSaveVerifier verifier;

	/**
	 * Public constructor
	 * <p>Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 * @param fileActions - constant from FileActions enum. 
	 * @param verifier - check whether the data should be saved
	 */
	public SaveFileAction(CoursesMain coursesMain,  FileActions fileActions, DataSaveVerifier verifier) {
		this.verifier = verifier;
		this.coursesMain = coursesMain;
		setText(SAVE);
		setId(fileActions.toString());
		setAccelerator(SWT.CTRL + 'S');
		setEnabled(false);
	}

	@Override
	public void run() {
		verifier.saveReportIfNeeded(coursesMain);
		performSaveCourse();
	}

	@Override
	public void handleEvent(Event event) {
		ActionStatusUpdater.updateEnableStatus(this, event, VIEWER_NOT_EMPTY, VIEWER_EMPTY);
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addReportEditorListener(this);
	}
	
	private void performSaveCourse() {
		FileDialog dlg = new FileDialog(coursesMain.getShell(), SWT.SAVE);

		dlg.setFilterNames(FILTER_NAMES);
		dlg.setFilterExtensions(FILTER_EXTENSIONS);

		if (dlg.open() != null) {
			String nameAndPath = String.format("%s\\%s", dlg.getFilterPath(), dlg.getFileName());
			try {
				coursesMain.getService().saveCourse(nameAndPath, coursesMain.getCourse());
				coursesMain.setShellText(nameAndPath);
			} catch (CourseManagerException exception) {
				Status status = new Status(IStatus.ERROR, PLUGIN_INFO, 0, WRONG_FILE_OR_DAMAGED, exception);

				new ErrorDialog(
						coursesMain.getShell(), 
						ERROR_SAVING_FILE,
						String.format(COUDNT_SAVE_MESSAGE, nameAndPath), 
						status, 
						IStatus.ERROR).open();
			}
		}
	}
}
