package com.luxoft.gui_jface.gui.viewer_sorter;

import java.util.*;
import java.util.function.BiFunction;

import org.eclipse.jface.viewers.*;

import com.luxoft.courses_api.models.Report;

/** 
 * Class extends org.eclipse.jface.viewers.ViewerComparator.
 * This class is used in ReportTableViewer. 
 * Sets the logic of comparing of each column of viewer
 * 
 * @author Georgiy Kucherenko
 *
 */
public class TableViewerSorter extends ViewerComparator {
	private static final int NAME_COLUMN = 0;
	private static final int GROUP_COLUMN = 1;
	private static final int DONE_COLUMN = 2;
	private static final int ASCENDING = 0;
	private static final int DESCENDING = 1;
	private BiFunction<Report, Report, Integer> compareNames;
	private BiFunction<Report, Report, Integer> compareGroups;
	private BiFunction<Report, Report, Integer> compareDone;
	private int column;
	private int direction;
	
	/**
	 * Public constructor 
	 * 
	 * @param comparator for comparing Strings
	 */
	public TableViewerSorter(Comparator<String> comparator) {
		compareNames = (a, b)-> comparator.compare(a.getName(), b.getName());
		compareGroups = (a, b)-> comparator.compare(a.getGroup(), b.getGroup());
		compareDone = (a, b)-> Boolean.compare(a.getDone(), b.getDone());
	}

	/**
	 * Method performing sorting. Accepts column number
	 * 
	 * @param column - number of column to be sorted.
	 */
	public void doSort(int column) {
		if (column == this.column) {
			direction = 1 - direction;
		} else {
			this.column = column;
			direction = ASCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object object1, Object object2) {
		int compareResult = 0;
		Report report1 = (Report) object1;
		Report report2 = (Report) object2;
		
		switch (column) {
		case NAME_COLUMN:
			compareResult = compareInOrder(report1, report2, List.of(
					compareNames, 
					compareGroups, 
					compareDone
					));
			break;
		case GROUP_COLUMN:
			compareResult = compareInOrder(report1, report2, List.of(
					compareGroups,
					compareNames, 
					compareDone
					));
			break;
		case DONE_COLUMN:
			compareResult = compareInOrder(report1, report2, List.of(
					compareDone,
					compareGroups,
					compareNames 
					));
			break;
		}
		
		if (direction == DESCENDING) {
			compareResult = -compareResult;
		}
		return compareResult;
	}
	
	private int compareInOrder(Report report1, Report report2, List<BiFunction<Report, Report, Integer>> comparers) {
		Integer compareResult = 0; 
		Iterator<BiFunction<Report, Report, Integer>> iter = comparers.iterator();
		
		while(iter.hasNext() && compareResult == 0) {
			compareResult = iter.next().apply(report1, report2);
		}
		
		return compareResult;
	}
}
