package com.luxoft.gui_jface.gui.actions.util;

/**
 * This interface has no methods. It holds constant variables for Actions
 * of CoursesMain object 
 * 
 * @author Georgiy Kucherenko
 *
 */
public interface ActionSettings {
	String DELETE = "Delete..";
	String SAVE = "Save..";
	String NEW = "New..";
	String NEW_REPORT = "New report";
	String SAVE_REPORT = "Save report";
	String CANCEL_REPORT = "Cancel editing";
	String DELETE_REPORT = "Delete report";
	String OPEN = "Open..";
	String CLOSE= "Close..";
	String FILE = "File";
	String CANCEL = "Cancel";
	String REPORTS = "Reports";
	String[] FILTER_NAMES = new String[] { "Course Files (*.crs)" };
	String[] FILTER_EXTENSIONS = new String[] { "*.crs" };
	String NEW_COURSE_MESSAGE = "New Course (is not saved yet)";
	String ERROR_OPENING_FILE = "Error opening file";
	String PLUGIN_INFO = "No plugin";
	String WRONG_FILE_OR_DAMAGED = "Wrong file name or file is damaged";
	String COUDNT_OPEN_MESSAGE = "Coudn't open file: %s";
	String ERROR_SAVING_FILE = "Error saving file";
	String COUDNT_SAVE_MESSAGE = "Coudn't save file: %s";
}
