package com.luxoft.gui_jface.gui.actions.util;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Event;

import com.luxoft.gui_jface.gui.models.ReportEditorActionType;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;

/**
 * This is Utility class. This class is used for updating enable/disable 
 * statuses of Actions (MenuItems)
 * 
 * @author Georgiy Kucherenko
 *
 */
public class ActionStatusUpdater {

	/**
	 * Changes enable status of Action in dependence of other parameters.
	 * 
	 * @param action - action to be modified
	 * @param event - instance of ReportEditorEvent
	 * @param enablingType - constant from enum ReportEditorActionType
	 * @param disablingType - constant from enum ReportEditorActionType
	 */
	public static void updateEnableStatus(Action action, Event event, ReportEditorActionType enablingType,
			ReportEditorActionType disablingType) {
		
		if (event instanceof ReportEditorEvent) {
			((ReportEditorEvent) event).getActionTypes().stream()
			.forEach(type -> {
				if (type == enablingType) {
					action.setEnabled(true);
				} else if (type == disablingType) {
					action.setEnabled(false);
				}
			});
		}
	}
}
