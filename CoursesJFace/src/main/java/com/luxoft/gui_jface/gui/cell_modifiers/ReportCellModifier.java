package com.luxoft.gui_jface.gui.cell_modifiers;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.Item;

import com.luxoft.courses_api.models.Report;
import com.luxoft.gui_jface.gui.main_view_composites.ReportTableViewerImpl;
import com.luxoft.gui_jface.gui.util.Procedure;

/**
 * This class is ICellModifier implementation for TableViewer of Courses App.
 * 
 * 
 * @author Georgiy Kucherenko
 *
 */
public class ReportCellModifier implements ICellModifier {
	private final Procedure VIEWER_UPDATER;
	

	/**
	 * Public constructor
	 * 
	 * @param tableViewerUpdater - supposed to be a method-reference for
	 * TableViewer updating
	 */
	public ReportCellModifier(Procedure tableViewerUpdater) {
		this.VIEWER_UPDATER = tableViewerUpdater;

	}

	@Override
	public boolean canModify(Object element, String property) {
		return true;
	}

	@Override
	public Object getValue(Object element, String property) {
		Report report = (Report) element;
		if (ReportTableViewerImpl.NAME.equals((property))) {
			return report.getName();
		} else if (ReportTableViewerImpl.GROUP.equals((property))) {
			return report.getGroup();
		} else if (ReportTableViewerImpl.DONE.equals((property))) {
			return report.getDone();
		}
		return null;
	}

	@Override
	public void modify(Object element, String property, Object value) {
		if (element instanceof Item) {
			element = ((Item) element).getData();
		} 
		
		if (!(element instanceof Item)) {
			return;
		}

		Report report = (Report) element;

		if (ReportTableViewerImpl.NAME.equals(property)) {
			if (report.getName().equals((String)value)) {
				return;
			}
			report.setName((String) value);
						
		} else if (ReportTableViewerImpl.GROUP.equals(property)) {
			if (report.getGroup().equals((String)value)) {
				return;
			} 
			report.setGroup((String) value);
			
		} else if (ReportTableViewerImpl.DONE.equals(property)) {
			if (report.getDone() == ((Boolean)value)) {
				return;
			} 
			report.setDone(((Boolean) value));
		}
		VIEWER_UPDATER.perform();
	}
}
