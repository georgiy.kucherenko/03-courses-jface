package com.luxoft.gui_jface.gui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.util.ActionStatusUpdater;
import com.luxoft.gui_jface.gui.listeners.*;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;
/**
 * This action creates a new Report object. 
 * In order to do it this action does following: 
 * <p><ul>
 * <li>calls notify method of appropriate GUI's button with new event as a parameter</li>
 * </ul>
 * 
 * @author Georgiy Kucherenko
 *
 */
public class NewReportAction extends Action implements Listener, SubMenuTuner{
	private final CoursesMain coursesMain;

	/**
	 * Public constructor
	 * <p>Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 */
	public NewReportAction(CoursesMain coursesMain) {
		this.coursesMain = coursesMain;
		setText(NEW_REPORT);
		setAccelerator(SWT.CTRL + SWT.ALT + 'N');
		setEnabled(false);
	}

	@Override
	public void run() {
		coursesMain.getReportEditor().notifyNewButtonListener(SWT.Selection, new Event());
	}
	
	@Override
	public void handleEvent(Event event) {
		ActionStatusUpdater.updateEnableStatus(this, event, ENABLE_NEW_BUTTON, DISABLE_NEW_BUTTON);
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addEnableSettingListenerToNewButton(this);
	}
}
