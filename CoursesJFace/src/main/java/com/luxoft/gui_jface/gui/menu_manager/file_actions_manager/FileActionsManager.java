package com.luxoft.gui_jface.gui.menu_manager.file_actions_manager;

import static java.util.Objects.isNull;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.action.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.listeners.SubMenuTuner;

/**
 * Object of this class helps to manipulate with menu items 
 * that are dedicated to work with Files. It is singleton class
 * 
 * @author Georgiy Kucherenko
 *
 */
public class FileActionsManager {
	private static final FileActionsManager ACTION_MANAGER = new FileActionsManager();
	private List<MenuItem> menuItems;

	private FileActionsManager() {
	}

	/**
	 * Method returning the only instance of this class
	 * 
	 * @return instance of FileActionsManager
	 */
	public static FileActionsManager getInstance() {
		return ACTION_MANAGER;
	}

	/**
	 * Method for finding the action by it's id
	 * Id is represented by enum FileActions
	 * 
	 * @param fileActions - value of FileActions enum
	 * @return - Action with given id (enum's value)
	 */
	public Action getAction(FileActions fileActions) {
		return (Action) menuItems.stream()
				.filter(menuItem -> compareFileActions(fileActions, menuItem))
				.findFirst()
				.map(menuItem -> ((ActionContributionItem) menuItem.getData()).getAction())
				.get();
	}

	/**
	 * Method changes statuses of FileActions
	 * 
	 * @param map - map of actions and booleans. Each action's 
	 * enable status changes according to boolean value
	 */
	public void updateActionFileStatuses(Map<FileActions, Boolean> map) {
		map.entrySet().stream()
		.filter(entry -> !isNull(entry.getKey()) && !isNull(entry.getValue()))
		.forEach(entry -> getAction(entry.getKey()).setEnabled(entry.getValue()));
	}

	/**
	 * Method initiates subscribing of actions to Widgets from ReportEditor. 
	 * 
	 * @param shell - active Shell
	 */
	public void tuneActions(Shell shell) {
		initiateItems(shell);
		
		menuItems.stream()
		.filter(this::instanceOfSubMenuTunerCheck)
		.forEach(this::tuneSubMenu);
	}
	
	/**
	 * Method initiates updating of menuItems field
	 */
	public void updateMenuItems() {
		initiateItems(Display.getCurrent().getActiveShell());
	}
	
	private void initiateItems(Shell shell) {
		menuItems = Arrays.stream(shell.getMenuBar().getItems())
				.flatMap(menuItem -> Arrays.stream(menuItem.getMenu().getItems()))
				.collect(Collectors.toList());
	}
	
	private boolean instanceOfSubMenuTunerCheck(MenuItem item) {
		return ((ActionContributionItem)item.getData()).getAction() instanceof SubMenuTuner;
	}
	
	private void tuneSubMenu(MenuItem item) {
		((SubMenuTuner)(((ActionContributionItem)item.getData()).getAction())).tuneListener();
	}
	
	private boolean compareFileActions(FileActions fileActions, MenuItem menuItem) {
		return fileActions.toString().equals(
				(((ActionContributionItem) menuItem.getData()).getAction()).getId());
	}
}
