package com.luxoft.gui_jface.gui.widget_providers;

import org.eclipse.jface.viewers.*;
import java.util.*;

/**
 * Instance of this class is a content provider for tableViewer that is used in CoursesMain class. 
 * 
 * @author Georgiy Kucherenko
 *
 */
public class TableViewerContentProvider implements IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		return ((List) inputElement).toArray();
	}
}
