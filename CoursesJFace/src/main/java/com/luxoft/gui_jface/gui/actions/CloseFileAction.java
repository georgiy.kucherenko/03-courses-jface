package com.luxoft.gui_jface.gui.actions;

import org.eclipse.jface.action.Action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.listeners.*;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.*;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;

import java.util.*;
import java.util.List;

/**
 * This action closes current Course object. 
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CloseFileAction extends Action  implements Listener, SubMenuTuner{
	private final CoursesMain coursesMain;
	private final DataSaveVerifier verifier;

	/**
	 * Public constructor
	 * <p>Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 * @param fileActions - constant from FileActions enum. 
	 * @param verifier - check whether the data should be saved
	 */
	public CloseFileAction(CoursesMain coursesMain,  FileActions fileActions, DataSaveVerifier verifier) {
		this.verifier = verifier;
		this.coursesMain = coursesMain;
		setText(CLOSE);
		setId(fileActions.toString());
		setAccelerator(SWT.CTRL + 'W');
		setEnabled(false);
	}

	@Override
	public void run() {
		verifier.saveReportIfNeeded(coursesMain);
		verifier.saveCourseIfNeeded(coursesMain);
		
		performCloseAction();
		updateFileMenuStatuses();
	}
	
	@Override
	public void handleEvent(Event event) {
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addReportEditorListener(this);
	}
	
	private void updateFileMenuStatuses() {
		FileActionsManager.getInstance().updateActionFileStatuses(Map.of(
				FileActions.SAVE, false,
				FileActions.NEW, true,
				FileActions.OPEN, true,
				FileActions.CLOSE, false
				));
	}

	private void performCloseAction() {
		coursesMain.getReportTableViewer().setInput(null);
		coursesMain.setShellText(null);
		coursesMain.setCourse(null);
		coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(List.of(
						DISABLE_CANCEL_BUTTON, 
						DISABLE_DELETE_BUTTON, 
						DISABLE_SAVE_BUTTON,
						DISABLE_NEW_BUTTON
						)));
	}
}
