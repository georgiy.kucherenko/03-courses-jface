package com.luxoft.gui_jface.gui.util;

/**
 * Functional interface. Doesn't accept parameters. Returns void
 * 
 * @author Georgiy Kucherenko
 *
 */
@FunctionalInterface
public interface Procedure {
	
	/**
	 * Method to be performed. No parameters. Returns void
	 */
	void perform();
}
