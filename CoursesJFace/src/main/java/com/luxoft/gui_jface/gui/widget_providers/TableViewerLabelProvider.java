package com.luxoft.gui_jface.gui.widget_providers;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import com.luxoft.courses_api.models.*;

/**
 * Instance of this class is a label provider for tableViewer that is used in CoursesMain class. 
 * 
 * @author Georgiy Kucherenko
 *
 */

public class TableViewerLabelProvider implements ITableLabelProvider {
	private static final int NAME_COLUMN = 0;
	private static final int GROUP_COLUMN = 1;
	private static final int DONE_COLUMN = 2;
	private final Image imageTrue = new Image(Display.getCurrent(), "true.png");
	private final Image imageFalse = new Image(Display.getCurrent(), "false.png");


	@Override
	public void dispose() {
		imageFalse.dispose();
		imageTrue.dispose();
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if (columnIndex == DONE_COLUMN) {
			return (((Report) element).getDone()) ? imageTrue : imageFalse;
		}
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		Report w = (Report) element;

		switch (columnIndex) {
			case NAME_COLUMN:
				return w.getName();
			case GROUP_COLUMN:
				return w.getGroup();
		}
		return "";
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		
	}
}
