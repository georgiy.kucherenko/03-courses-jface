package com.luxoft.gui_jface.gui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.util.ActionStatusUpdater;
import com.luxoft.gui_jface.gui.listeners.*;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.*;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;
import static java.util.Objects.isNull;

import java.util.*;
import java.util.List;

/**
 * This action creates a new Course object. 
 * In order to do it this action does following: 
 * <p><ul>
 * <li>Communicates with Courses API through CoursesMain object</li>
 * </ul>
 * 
 * @author Georgiy Kucherenko
 *
 */
public class NewFileAction extends Action  implements Listener, SubMenuTuner{
	private final CoursesMain coursesMain;
	private final DataSaveVerifier verifier;

	/**
	 * Public constructor
	 * <p>Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 * @param fileActions - constant from FileActions enum. 
	 * @param verifier - check whether the data should be saved
	 */
	public NewFileAction(CoursesMain coursesMain,  FileActions fileActions, DataSaveVerifier verifier) {
		this.verifier = verifier;
		this.coursesMain = coursesMain;
		setText(NEW);
		setId(fileActions.toString());
		setAccelerator(SWT.CTRL + 'N');	
	}

	@Override
	public void run() {

		if (!isNull(coursesMain.getCourse())) {
			verifier.saveReportIfNeeded(coursesMain);
			verifier.saveCourseIfNeeded(coursesMain);
		}
		
		performNewFileAction();
		updateFileMenuStatuses();
	}

	@Override
	public void handleEvent(Event event) {
		ActionStatusUpdater.updateEnableStatus(this, event, VIEWER_NOT_EMPTY, VIEWER_EMPTY);
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addReportEditorListener(this);
	}
	
	private void updateFileMenuStatuses() {
		FileActionsManager.getInstance().updateActionFileStatuses(Map.of(
				FileActions.SAVE, false,
				FileActions.NEW, false,
				FileActions.CLOSE, true
				));
	}

	private void performNewFileAction() {
		coursesMain.setCourse(coursesMain.getService().createCourse());
		coursesMain.getReportTableViewer().setInput(coursesMain.getCourse().getReportList());
		coursesMain.setShellText(NEW_COURSE_MESSAGE);
		coursesMain.getReportEditor().notifyReportEditor(new ReportEditorEvent(List.of(ENABLE_NEW_BUTTON)));
	}
}
