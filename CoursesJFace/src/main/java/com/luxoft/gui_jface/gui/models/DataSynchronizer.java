package com.luxoft.gui_jface.gui.models;

import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;
import static java.util.Objects.isNull;

import java.util.*;
import java.util.function.Consumer;

import com.luxoft.courses_api.models.Report;

/**
 * Object of this class synchronizes processes between Table in GUI
 * and GUI's text fields and checkBox. It is also involved in 
 * manipulating with data like saving and deleting data etc.
 * Also state of object stores current report and previous report.
 * This helps to undo changes etc
 * 
 * @author Georgiy Kucherenko
 *
 */
public class DataSynchronizer {
	public static final Report BLANK_REPORT = new Report("", "", false);
	private Report currentReport;
	private Report previousReport;
	private Boolean mustBeSaved = false;
	private Consumer<ReportEditorEvent> editorNotifier;

	/**
	 * Public constructor
	 * 
	 * @param eventNotifier - Consumer. Supposed to be a method reference 
	 * for notifying ReportEditor object 
	 */
	public DataSynchronizer(Consumer<ReportEditorEvent> eventNotifier) {
		this.editorNotifier = eventNotifier;
	}

	/**
	 * Setter for current Report
	 * 
	 * @param report - report to be set
	 */
	public void setCurrentReport(Report report) {
		this.currentReport = report;
	}

	/**
	 * Setter for mustBeSaved
	 * 
	 * @param mustBeSaved - true means that current report should be 
	 * saved before closing. False means that report can be closed without saving
	 */
	public void setMustBeSaved(Boolean mustBeSaved) {
		this.mustBeSaved = mustBeSaved;
	}

	/**
	 * Getter for currentReport
	 * 
	 * @return value of currently displayed report
	 */
	public Report getCurrentReport() {
		return currentReport;
	}

	/**
	 * Getter for previousReport
	 * 
	 * @return previousReport. It is either blank report or 
	 * a report that was opened before editing began
	 */
	public Report getPreviousReport() {
		return previousReport;
	}

	/**
	 * Getter. Returns weather currentReport should be saved before closing
	 * 
	 * @return - true if report must be saved before closing. Otherwise - returns false
	 */
	public Boolean getMustBeSaved() {
		return mustBeSaved;
	}
	
	/**
	 * Method for creating BlankReport (empty report)
	 */
	public void createNewReport() {
		openReport(provideBlankReport());
	}
	
	/**
	 * Method for opening report
	 * 
	 * @param report - report to be opened
	 */
	public void openReport(Report report) {
		if (isNull(previousReport)) {
			showButtons();
		}
		
		showReport(report);
	}

	/**
	 * Updates current state of Synchronizer with provided report.
	 * Also initiates updates of widgets in UI
	 * 
	 * @param report
	 */
	public void setNewStateOfSynchronizer(Report report) {
		modifyState(report);
		updateEditingPermissions();
	}
	
	/**
	 * Initiates displaying provided report in UI. 
	 * Also updates state of synchronizer objec
	 * 
	 * @param report - report to be shown
	 */
	public void showReport(Report report) {
		modifyState(report);
		editorNotifier.accept(new ReportEditorEvent(REPORT_TO_BE_SET, report));
		updateEditingPermissions();
	}
	
	/**
	 * Method resets state of Synchronizer to null.
	 * Also initiates hiding buttons
	 */
	public void performActionsAfterDelete() {
		resetStateToNull();
		disableAndHideAllButtons();
	}
	
	/**
	 * Method resets state of synchronizer to null
	 */
	public void resetStateToNull() {
		modifyState(null);
	}
	/**
	 * Method resets currentReport to previousReport
	 */
	public void doCancel() {
		showReport(previousReport);
	}

	/**
	 * Method updates currentReport with provided report.
	 * 
	 * @param displayedReport - data from UI
	 */
	public void updateCurrentReport(Report displayedReport) {
		currentReport = displayedReport.clone();
		updateEditingPermissions();
	}
	
	private final Report provideBlankReport() {
		return new Report("", "", Boolean.valueOf(false));
	}
	
	private void modifyState(Report report) {
		previousReport = !isNull(report) ? report.clone() : null;
		currentReport = !isNull(report) ? report.clone() : null;
		mustBeSaved = false;
	}
	
	private void updateEditingPermissions() {

		//case when..: created blank report and it is blank still
		if (previousReport.equals(BLANK_REPORT) && (currentReport.equals(BLANK_REPORT))) {
			mustBeSaved = true;
			editorNotifier.accept(new ReportEditorEvent(List.of(
							DISABLE_SAVE_BUTTON, 
							ENABLE_DELETE_BUTTON,
							DISABLE_CANCEL_BUTTON
							)));
			
			//case when..: name field or group field is empty
		} else if(currentReport.getName().isBlank() || currentReport.getGroup().isBlank()) {
			mustBeSaved=true;
			editorNotifier.accept(new ReportEditorEvent(List.of(
							ENABLE_CANCEL_BUTTON, 
							ENABLE_DELETE_BUTTON,
							DISABLE_SAVE_BUTTON
							)));	

			//case when..: created blank report and made corrections
		}else if (previousReport.equals(BLANK_REPORT)&& !currentReport.equals(BLANK_REPORT) ) {
			mustBeSaved=true;
			editorNotifier.accept(new ReportEditorEvent(List.of(
							ENABLE_SAVE_BUTTON, 
							ENABLE_DELETE_BUTTON,
							ENABLE_CANCEL_BUTTON
							)));
			
			//case when..: beginning state is equal to current state 
		} else if (currentReport.equals(previousReport)) {
			mustBeSaved = false;
			editorNotifier.accept(new ReportEditorEvent(List.of(
							DISABLE_SAVE_BUTTON, 
							ENABLE_DELETE_BUTTON,
							DISABLE_CANCEL_BUTTON
							)));
			
			//when opened some report and made corrections
		} else if (!currentReport.equals(previousReport)){
			mustBeSaved = true;
			editorNotifier.accept(new ReportEditorEvent(List.of(
							ENABLE_CANCEL_BUTTON, 
							ENABLE_DELETE_BUTTON,
							ENABLE_SAVE_BUTTON
							)));
		}
	}

	private void disableAndHideAllButtons() {
		editorNotifier.accept(new ReportEditorEvent(List.of(
						DISABLE_SAVE_BUTTON, 
						DISABLE_CANCEL_BUTTON,
						DISABLE_DELETE_BUTTON,
						SHOW_WIDGETS_FALSE
						)));
	}

	private void showButtons() {
		editorNotifier.accept(new ReportEditorEvent(List.of(
						SHOW_WIDGETS_TRUE,
						ENABLE_DELETE_BUTTON
						)));
	}
}
