package com.luxoft.gui_jface.gui.menu_manager;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;

import org.eclipse.jface.action.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.*;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.*;

/**
 * Instance of this class represents a MenuManager for CoursesMain object. 
 * No public methods. Only constructor is public.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CoursesMenuManager extends MenuManager {

	/** 
	 * Public constructor. 
	 * 
	 * @param coursesMain - an object whom this instance belongs to
	 */
	public CoursesMenuManager(CoursesMain coursesMain) {
		createFileMenu(coursesMain);
		createReportsMenu(coursesMain);
	}
	
	/**
	 * Method for getting action from FileActionsManager
	 * 
	 * @param action - FileActions enum object
	 * @return requested action
	 */
	public Action getAction(FileActions action) {
		return FileActionsManager.getInstance().getAction(action);
	}

	private void createFileMenu(CoursesMain coursesMain) {
		MenuManager fileMenuManager = new MenuManager(FILE);
		add(fileMenuManager);
		DataSaveVerifier verifier = DataSaveVerifier.getInstance();

		fileMenuManager.add(new NewFileAction(coursesMain, FileActions.NEW, verifier));
		fileMenuManager.add(new OpenFileAction(coursesMain, FileActions.OPEN, verifier));
		fileMenuManager.add(new SaveFileAction(coursesMain, FileActions.SAVE, verifier));
		fileMenuManager.add(new CloseFileAction(coursesMain, FileActions.CLOSE,  verifier));
	}

	private void createReportsMenu(CoursesMain coursesMain) {
		MenuManager reportsMenuManager = new MenuManager(REPORTS);
		add(reportsMenuManager);
		
		reportsMenuManager.add(new NewReportAction(coursesMain));
		reportsMenuManager.add(new SaveReportAction(coursesMain));
		reportsMenuManager.add(new CancelReportAction(coursesMain));
		reportsMenuManager.add(new DeleteReportAction(coursesMain));
	}
}
