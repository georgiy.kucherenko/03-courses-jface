package com.luxoft.gui_jface.gui.main_view_composites;

import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;
import static java.util.Objects.isNull;

import java.util.*;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import com.luxoft.courses_api.models.Report;
import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.models.DataSynchronizer;
import com.luxoft.gui_jface.gui.models.ReportEditorActionType;
import com.luxoft.gui_jface.gui.models.ReportEditorEvent;

/**
 * Instance of this class is an integral part of CoursesMain object. It extends
 * Composite class. And represents content of right part of user interface
 * (CoursesMain class). It encapsulates 3 widgets for data input and output, and
 * 4 more widgets (buttons) for manipulating the data.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class ReportEditor extends Composite implements Listener {
	private static final int EDITOR_NOTIFIER = 777;
	private Text nameText;
	private Text groupText;
	private Button checkDoneButton;
	private Button deleteButton;
	private Button cancelButton;
	private Button newButton;
	private Button saveButton;
	private DataSynchronizer synchronizer = new DataSynchronizer(this::notifyReportEditor);
	private ModifyListener modifyListener; 
	private SelectionListener selectionListener; 
	private CoursesMain coursesMain;

	/**
	 * Public constructor
	 * 
	 * @param parent      - instance of SashForm class from CoursesMain object
	 * @param coursesMain - instance of CoursesMain
	 */
	public ReportEditor(SashForm parent, CoursesMain coursesMain) {
		super(parent, SWT.NONE);
		this.coursesMain = coursesMain;
		setLayout(new FillLayout());
		setupComposite();
		setupListeners();
		addListener(EDITOR_NOTIFIER, this);
		tuneComponentsVisibility();
	}

	/**
	 * Wrapping method. Calls notifyListeners method of deleteButton
	 * 
	 * @param eventType - the type of event which has occurred
	 * @param event     - event data
	 */
	public void notifyDeleteButtonListener(int eventType, Event event) {
		deleteButton.notifyListeners(eventType, event);
	}

	/**
	 * Wrapping method. Calls notifyListeners method of newButton
	 * 
	 * @param eventType - the type of event which has occurred
	 * @param event     - event data
	 */
	public void notifyNewButtonListener(int eventType, Event event) {
		newButton.notifyListeners(eventType, event);
	}

	/**
	 * Wrapping method. Calls notifyListeners method of saveButton
	 * 
	 * @param eventType - the type of event which has occurred
	 * @param event     - event data
	 */
	public void notifySaveButtonListener(int eventType, Event event) {
		saveButton.notifyListeners(eventType, event);
	}

	/**
	 * Wrapping method. Calls notifyListeners method of cancelButton
	 * 
	 * @param eventType - the type of event which has occurred
	 * @param event     - event data
	 */
	public void notifyCancelButtonListener(int eventType, Event event) {
		cancelButton.notifyListeners(eventType, event);
	}

	/**
	 * Wrapping method. Sets the text value to nameText element
	 * 
	 * @param text - text to be set
	 */
	public void setNameTextValue(String text) {
		nameText.setText(text);
	}

	/**
	 * Wrapping method. Sets the text value to groupText element
	 * 
	 * @param text - text to be set
	 */
	public void setGroupTextValue(String text) {
		groupText.setText(text);
	}

	/**
	 * Wrapping method. Sets the selection status to checkDoneButton element.
	 * 
	 * @param selected - value to be set
	 */
	public void setSelectionOfCheckDoneButton(boolean selected) {
		checkDoneButton.setSelection(selected);
	}
	
	/**
	 * Adds listener to ReportEditor class
	 * 
	 * @param listener - listener to be added
	 */
	public void addReportEditorListener(Listener listener) {
		addListener(EDITOR_NOTIFIER, listener);
	}

	@Override
	public void handleEvent(Event event) {
		if(!(event instanceof ReportEditorEvent)) {
			return;
		}

		for (ReportEditorActionType actionType : ((ReportEditorEvent) event).getActionTypes()) {
			
			if (actionType == VIEWER_SELECTION_CHANGED) {
				handleViewerSelectionChanged();
				
			} else if (actionType == EDITOR_DATA_CHANGED) {
				synchronizer.updateCurrentReport(getReportFromWidgets());
				
			} else if (actionType == CHECK_IF_NEED_TO_SAVE_REPORT) {
				shouldSaveCheck();
				
			} else if ((actionType == REPORT_TO_BE_SET)) {
				setReportToWidgets(((ReportEditorEvent) event).getReport());
				
			} else if (actionType == REMOVE_MODIFY_LISTENER) {
				removeListenersFromWidgets();
				
			} else if (actionType == ADD_MODIFY_LISTENER) {
				addListenersToWidgets();
				
			} else if (actionType == SHOW_WIDGETS_TRUE) {
				setWidgetsVisibility(true);
				
			} else if (actionType == SHOW_WIDGETS_FALSE) {
				setWidgetsVisibility(false);
				
			} else if (actionType == RESET_STATE) {
				resetState();
				
			} else if (actionType == ENABLE_CANCEL_BUTTON) {
				updateAndNotifyButton(cancelButton, true, actionType);
				
			} else if ((actionType == DISABLE_CANCEL_BUTTON)) {
				updateAndNotifyButton(cancelButton, false, actionType);
				
			} else if (actionType == ENABLE_SAVE_BUTTON) {
				updateAndNotifyButton(saveButton, true, actionType);
				
			} else if ((actionType == DISABLE_SAVE_BUTTON)) {
				updateAndNotifyButton(saveButton, false, actionType);
				
			} else if (actionType == ENABLE_DELETE_BUTTON) {
				updateAndNotifyButton(deleteButton, true, actionType);
				
			} else if ((actionType == DISABLE_DELETE_BUTTON)) {
				updateAndNotifyButton(deleteButton, false, actionType);
				
			} else if (actionType == ENABLE_NEW_BUTTON) {
				updateAndNotifyButton(newButton, true, actionType);
				
			} else if ((actionType == DISABLE_NEW_BUTTON)) {
				updateAndNotifyButton(newButton, false, actionType);
			}
		}
		coursesMain.setMessageToStatusLineManager(
				String.format("%s%s", STATUS_LINE_MESSAGE, synchronizer.getMustBeSaved()));

	}
	
	/**
	 * Notifies all of the receiver's listeners for events
	 * of the given type
	 * 
	 * @param reportEditorEvent - event to be notified with
	 */
	public void notifyReportEditor(ReportEditorEvent reportEditorEvent) {
		notifyListeners(EDITOR_NOTIFIER, reportEditorEvent);
	}

	/**
	 * Adds listener to widget. 
	 * 
	 * @param listener - listener to be added
	 */
	public void addEnableSettingListenerToSaveButton(Listener listener) {
		saveButton.addListener(EDITOR_NOTIFIER, listener);
	}

	/**
	 * Adds listener to widget. 
	 * 
	 * @param listener - listener to be added
	 */
	public void addEnableSettingListenerToCancelButton(Listener listener) {
		cancelButton.addListener(EDITOR_NOTIFIER, listener);
	}

	/**
	 * Adds listener to widget. 
	 * 
	 * @param listener - listener to be added
	 */
	public void addEnableSettingListenerToDeleteButton(Listener listener) {
		deleteButton.addListener(EDITOR_NOTIFIER, listener);
	}
	
	/**
	 * Adds listener to widget. 
	 * 
	 * @param listener - listener to be added
	 */
	public void addEnableSettingListenerToNewButton(Listener listener) {
		newButton.addListener(EDITOR_NOTIFIER, listener);
	}
	
	private void tuneComponentsVisibility() {
		setWidgetsVisibility(false);
		notifyReportEditor(new ReportEditorEvent(List.of(
						DISABLE_CANCEL_BUTTON,
						DISABLE_DELETE_BUTTON, 
						DISABLE_SAVE_BUTTON,
						DISABLE_NEW_BUTTON
						)));
	}

	private void setupComposite() {
		Composite wigetsComposite = new Composite(this, SWT.NONE);
		wigetsComposite.setLayout(new GridLayout(4, false));

		createNameLabel(wigetsComposite);
		createNameText(wigetsComposite);
		createGroupLabel(wigetsComposite);
		createGroupText(wigetsComposite);
		createDoneTaskLabel(wigetsComposite);
		createCheckDoneButton(wigetsComposite);
		createNewButton(wigetsComposite);
		createSaveButton(wigetsComposite);
		createCancelButton(wigetsComposite);
		createDeleteButton(wigetsComposite);
	}

	private void createDeleteButton(Composite wigetsComposite) {
		deleteButton = new Button(wigetsComposite, SWT.BORDER);
		setupButton(deleteButton, DELETE);
	}

	private void createCancelButton(Composite wigetsComposite) {
		cancelButton = new Button(wigetsComposite, SWT.BORDER);
		setupButton(cancelButton, CANCEL);
	}

	private void createSaveButton(Composite wigetsComposite) {
		saveButton = new Button(wigetsComposite, SWT.BORDER);
		setupButton(saveButton, SAVE);
	}

	private void createNewButton(Composite wigetsComposite) {
		newButton = new Button(wigetsComposite, SWT.BORDER | SWT.PUSH);
		setupButton(newButton, NEW);
	}

	private void setupButton(Button button, String buttonText) {
		button.setText(buttonText);
		GridData gData = new GridData(GridData.FILL_BOTH);
		gData.verticalAlignment = SWT.END;
		button.setLayoutData(gData);
	}

	private void createCheckDoneButton(Composite wigetsComposite) {
		checkDoneButton = new Button(wigetsComposite, SWT.CHECK | SWT.RIGHT | SWT.RIGHT_TO_LEFT);
		checkDoneButton.setText("");

		GridData gData = new GridData();
		gData.horizontalAlignment = SWT.RIGHT;
		checkDoneButton.setLayoutData(gData);
	}

	private void createDoneTaskLabel(Composite wigetsComposite) {
		Label doneTaskLabel = new Label(wigetsComposite, SWT.SINGLE | SWT.LEFT);
		doneTaskLabel.setText(SWT_TASK_DONE);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		gData.horizontalSpan = 3;
		doneTaskLabel.setLayoutData(gData);
	}

	private void createGroupText(Composite wigetsComposite) {
		groupText = new Text(wigetsComposite, SWT.SINGLE | SWT.BORDER | SWT.RIGHT);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		gData.horizontalSpan = 3;
		groupText.setLayoutData(gData);
	}

	private void createGroupLabel(Composite wigetsComposite) {
		Label groupLabel = new Label(wigetsComposite, SWT.SINGLE | SWT.LEFT);
		groupLabel.setText(GROUP);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		groupLabel.setLayoutData(gData);
	}

	private void createNameText(Composite wigetsComposite) {
		nameText = new Text(wigetsComposite, SWT.SINGLE | SWT.BORDER | SWT.RIGHT);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		gData.horizontalSpan = 3;
		nameText.setLayoutData(gData);
	}

	private void createNameLabel(Composite wigetsComposite) {
		Label nameLabel = new Label(wigetsComposite, SWT.SINGLE | SWT.LEFT);
		nameLabel.setText(NAME);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		nameLabel.setLayoutData(gData);
	}

	private void setupListeners() {
		deleteButton.addSelectionListener(createDeleteButtonListener());
		cancelButton.addSelectionListener(createCancelButtonListener());
		saveButton.addSelectionListener(createSaveButtonListener());
		newButton.addSelectionListener(createNewButtonListener());
		modifyListener = createModifyListener(this);
		selectionListener = createSelectionListener(this);
		nameText.addModifyListener(modifyListener);
		groupText.addModifyListener(modifyListener);
		checkDoneButton.addSelectionListener(selectionListener);
	}

	private SelectionListener createSelectionListener(ReportEditor reportEditor) {
		return SelectionListener.widgetSelectedAdapter((a)->{
			reportEditor.notifyReportEditor(new ReportEditorEvent(EDITOR_DATA_CHANGED));
		});
	}

	private ModifyListener createModifyListener(ReportEditor reportEditor) {
		return (a)-> reportEditor.notifyReportEditor(new ReportEditorEvent(EDITOR_DATA_CHANGED)); 
	}

	private void clearFields() {
		nameText.setText("");
		groupText.setText("");
		checkDoneButton.setSelection(false);
	}

	private SelectionListener createCancelButtonListener() {
		return SelectionListener.widgetSelectedAdapter((a)->synchronizer.doCancel()); 
	}

	private SelectionListener createDeleteButtonListener() {
		return SelectionListener.widgetSelectedAdapter((a) -> {
				TableItem[] items = coursesMain.getReportTableViewer().getSelectionOfTable();

				if (!isNull(items) && items.length > 0) {
					coursesMain.getCourse().removeReport((Report) items[0].getData());
					coursesMain.getReportTableViewer().remove(items[0]);
					coursesMain.getReportTableViewer().refresh();
					clearFields();
				}
				synchronizer.performActionsAfterDelete();
			
		});
	}

	private SelectionListener createSaveButtonListener() {
		return SelectionListener.widgetSelectedAdapter((a)-> {
				Report report = saveReport();
				doSelectionAfterSave(report);
			});
	}

	private Report saveReport() {
		Report report = null;

		if (!DataSynchronizer.BLANK_REPORT.equals(synchronizer.getPreviousReport())) {
			TableItem itemToBeSaved = findTableItemByReport(synchronizer.getPreviousReport());

			if (!isNull(itemToBeSaved)) {
				report = (Report) itemToBeSaved.getData();
				report.setValues(getReportFromWidgets());
			}
		} else {
			report = getReportFromWidgets();
			coursesMain.getReportTableViewer().addReport(report);
			coursesMain.getCourse().addReport(report);
		}
		coursesMain.getReportTableViewer().refresh();
		return report;
	}

	private void doSelectionAfterSave(Report report) {
		synchronizer.setNewStateOfSynchronizer(report);
		coursesMain.getReportTableViewer().setSelectionOfTable(findTableItemByReport(report));
		nameText.setFocus();
	}
	
	private SelectionListener createNewButtonListener() {
		return SelectionListener.widgetSelectedAdapter((a)->{
				createNewReport();
				nameText.setFocus();
		});
	}


	private void updateAndNotifyButton(Button button, boolean enabeled, ReportEditorActionType type) {
		button.setEnabled(enabeled);
		button.notifyListeners(EDITOR_NOTIFIER, new ReportEditorEvent(type));
	}
	
	private void resetState() {
		setWidgetsVisibility(false);
		synchronizer.resetStateToNull();;
		coursesMain.getReportTableViewer().setSelectionToNull();
	}
	
	private void setPreviousReportToViewer() {
		removeListenersFromWidgets();

		if (DataSynchronizer.BLANK_REPORT.equals(synchronizer.getPreviousReport())) {
			coursesMain.getReportTableViewer().setSelectionToNull();
		} else {
			coursesMain.getReportTableViewer()
			.setSelectionOfTable(findTableItemByReport(synchronizer.getPreviousReport()));
		}
		
		addListenersToWidgets();
	}
	

	private Report getReportFromWidgets() {
		return new Report(nameText.getText(), groupText.getText(), checkDoneButton.getSelection());
	}

	private void openReportFromTableViewer() {
		synchronizer.openReport(getSelectedFromTableViewer());
	}

	private void removeListenersFromWidgets() {
		nameText.removeModifyListener(modifyListener);
		groupText.removeModifyListener(modifyListener);
		checkDoneButton.removeSelectionListener(selectionListener);
	}

	private void addListenersToWidgets() {
		nameText.addModifyListener(modifyListener);
		groupText.addModifyListener(modifyListener);
		checkDoneButton.addSelectionListener(selectionListener);
	}

	private void setReportToWidgets(Report report) {
		removeListenersFromWidgets();

		nameText.setText(report.getName());
		groupText.setText(report.getGroup());
		checkDoneButton.setSelection(report.getDone());

		addListenersToWidgets();
	}

	private Report getSelectedFromTableViewer() {
		TableItem[] items = coursesMain.getReportTableViewer().getSelectionOfTable();
		return (!isNull(items) && items.length > 0) ? ((Report) items[0].getData()) : null; 
	}
	
	private Boolean queryShoudSaveChanges() {
		return MessageDialog.openQuestion(getShell(), DIALOG_QUESTION, DIALOG_QUESTION_MESSAGE);
	}

	private void setWidgetsVisibility(boolean shouldHide) {
		removeListenersFromWidgets();
		nameText.setVisible(shouldHide);
		groupText.setVisible(shouldHide);
		checkDoneButton.setVisible(shouldHide);
		addListenersToWidgets();
	}

	private TableItem findTableItemByReport(Report report) {
		TableItem[] items = coursesMain.getReportTableViewer().getItemsOfTable();
		
		return Arrays.stream(items)
				.filter((tableItem)-> ((Report) tableItem.getData()).equals(report))
				.findFirst()
				.get();
	}

	private void shouldSaveCheck() { 
		if (synchronizer.getMustBeSaved() && queryShoudSaveChanges() && verifyInputData()) {
			saveReport();
		}
	}

	private void createNewReport() {
		showWidgetsIfHidden();

		if (synchronizer.getMustBeSaved() && queryShoudSaveChanges()) {
			if (!verifyInputData()) {
				return;
			}
			
			saveReport();
		}
		coursesMain.getReportTableViewer().setSelectionToNull();
		synchronizer.createNewReport();
	}

	private void showWidgetsIfHidden() {
		if (isNull(synchronizer.getCurrentReport())) {
			setWidgetsVisibility(true);
		}	
	}
	
	private void handleViewerSelectionChanged() {
		showWidgetsIfHidden();

		if (synchronizer.getMustBeSaved() && queryShoudSaveChanges()) {
			if (!verifyInputData()) {
				setPreviousReportToViewer();
				return;
			}

			saveReport();
		}
		openReportFromTableViewer();
	}
	
	private boolean verifyInputData() {
		if (DataSynchronizer.BLANK_REPORT.equals(getReportFromWidgets())) {
			MessageDialog.openWarning(getShell(), WARNING, CANNOT_SAVE_EMPTY_REPORT);
			return false;
		} else if (nameText.getText().isEmpty()) {
			MessageDialog.openWarning(getShell(), WARNING, CANNOT_SAVE_WITHOUT_NAME);
			return false;
		} else if (groupText.getText().isEmpty()) {
			MessageDialog.openWarning(getShell(), WARNING, CANNOT_SAVE_WITHOUT_GROUP);
			return false;
		}
		return true;
	}
	
	private static final String DELETE = "Delete";
	private static final String CANCEL = "Cancel";
	private static final String SAVE = "Save";
	private static final String NEW = "New";
	private static final String SWT_TASK_DONE = "SWT Task done";
	private static final String GROUP = "Group";
	private static final String NAME = "Name";
	private static final String DIALOG_QUESTION = "Question";
	private static final String DIALOG_QUESTION_MESSAGE = "Should we save changes in REPORT?";
	private static final String WARNING = "Warning!";
	private static final String CANNOT_SAVE_EMPTY_REPORT = "Cannot save empty report";
	private static final String CANNOT_SAVE_WITHOUT_NAME = "Name field shoudn't be empty";
	private static final String CANNOT_SAVE_WITHOUT_GROUP = "Group field shoudn't be empty";
	private static final String STATUS_LINE_MESSAGE = "Report editing status: ";
}
