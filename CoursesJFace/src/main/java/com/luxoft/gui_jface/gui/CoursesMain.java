package com.luxoft.gui_jface.gui;

import static java.util.Objects.isNull;

import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

import com.luxoft.courses_api.models.Course;
import com.luxoft.courses_api.service.SchoolService;
import com.luxoft.gui_jface.gui.main_view_composites.*;
import com.luxoft.gui_jface.gui.menu_manager.*;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.FileActions;
import com.luxoft.gui_jface.gui.menu_manager.file_actions_manager.FileActionsManager;

/**
 * This is main GUI class for Courses application. 
 * It works with Courses API. So it must receive an instance 
 * of Courses API object through the constructor (SchoolService class)
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CoursesMain extends ApplicationWindow {
	private static final String DIALOG_QUESTION = "Please reply";
	private static final String DIALOG_QUESTION_MESSAGE = "Would you like to save changes in COURSE before quiting?";
	public static final String TABLE_COURSES = "Table Courses";
	private SchoolService service;
	private ReportTableViewer reportTableViewer;
	private ReportEditor reportEditor;
	private Course course;
	private StatusLineManager statusLineManager;

	/**
	 * Public constructor
	 * 
	 * @param service - and instance of Courses API object
	 */
	public CoursesMain(SchoolService service) {
		super(null);
		this.service = service;
		addMenuBar();
		addStatusLine();
	}

	/**
	 * This method starts (opens) GUI application
	 */
	public void run() {
		setBlockOnOpen(true);
		open();
		Display.getCurrent().dispose();
	}

	/**
	 * Getter 
	 * 
	 * @return - SchoolService object
	 */
	public SchoolService getService() {
		return service;
	}

	/** Getter
	 * 
	 * @return the composite that holds tableViewer
	 */
	public ReportTableViewer getReportTableViewer() {
		return reportTableViewer;
	}

	/** Getter
	 * 
	 * @return the composite that widgets for manipulating with data
	 */
	public ReportEditor getReportEditor() {
		return reportEditor;
	}

	/**
	 * Getter 
	 * 
	 * @return the data that User is currently working with
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * Setter 
	 * 
	 * @param course sets the data to work with
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * Wrapping method
	 * 
	 * @param text - message to be set in StatusLineManager
	 */
	public void setMessageToStatusLineManager(String text) {
		statusLineManager.setMessage(text);
	}
	
	/**
	 * Sets the text of main window of Courses app
	 * 
	 * @param pathOfFile - path of file that is currently opened.
	 */
	public void setShellText(String pathOfFile) {
		getShell().setText(
				isNull(pathOfFile) ?
						TABLE_COURSES : 
						String.format("%s - %s", TABLE_COURSES, pathOfFile)
				);
	}
	
	@Override
	protected void handleShellCloseEvent() {
		if (!isNull(course) && course.getReportList().size() > 0  
			&& MessageDialog.openQuestion(getShell(), DIALOG_QUESTION, DIALOG_QUESTION_MESSAGE)) {
			
			((CoursesMenuManager)getMenuBarManager()).getAction(FileActions.SAVE).run();
		}
		
		super.handleShellCloseEvent();
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite sashForm = setupSashForm(parent);
		tuneMenus();
		return sashForm;
	}

	@Override
	protected StatusLineManager createStatusLineManager() {
		return statusLineManager = new StatusLineManager();
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);

		shell.setText(TABLE_COURSES);
		Point size = new Point(600, 300);
		shell.setSize(size);
		shell.setMinimumSize(size);
	}

	@Override
	protected MenuManager createMenuManager() {
		return new CoursesMenuManager(this);
	}

	private Composite setupSashForm(Composite composite) {
		SashForm sashForm = new SashForm(composite, SWT.HORIZONTAL | SWT.BORDER);
		Composite tableViewerComposite = new Composite(sashForm, SWT.NONE);
		tableViewerComposite.setLayout(new FillLayout());
		reportTableViewer = new ReportTableViewerImpl(tableViewerComposite, SWT.FULL_SELECTION, this);
		reportEditor = new ReportEditor(sashForm, this);
		sashForm.setWeights(new int[] { 5, 5 });
		return sashForm;
	}

	private void tuneMenus() {
		FileActionsManager.getInstance().tuneActions(getShell());
	}
}
