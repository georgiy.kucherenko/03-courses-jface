package com.luxoft.gui_jface.gui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

import static com.luxoft.gui_jface.gui.actions.util.ActionSettings.*;
import static com.luxoft.gui_jface.gui.models.ReportEditorActionType.*;

import com.luxoft.gui_jface.gui.CoursesMain;
import com.luxoft.gui_jface.gui.actions.util.ActionStatusUpdater;
import com.luxoft.gui_jface.gui.listeners.SubMenuTuner;

/**
 * This action cancels modifying of Report object in GUI. In order to cancel it
 * this action does following:
 * <p>
 * <ul>
 * <li>calls notify method of appropriate GUI's button with new event as a
 * parameter</li>
 * </ul>
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CancelReportAction extends Action implements Listener, SubMenuTuner {
	private final CoursesMain coursesMain;

	/**
	 * Public constructor
	 * <p>
	 * Also sets the accelerator keycode and action's name.
	 * 
	 * @param coursesMain - instance of main GUI class - CoursesMain class
	 */
	public CancelReportAction(CoursesMain coursesMain) {
		this.coursesMain = coursesMain;
		setText(CANCEL_REPORT);
		setAccelerator(SWT.CTRL + SWT.ALT + 'C');
		setEnabled(false);
	}

	@Override
	public void run() {
		coursesMain.getReportEditor().notifyCancelButtonListener(SWT.Selection, new Event());
	}

	@Override
	public void handleEvent(Event event) {
		ActionStatusUpdater.updateEnableStatus(this, event, ENABLE_CANCEL_BUTTON, DISABLE_CANCEL_BUTTON);
		
	}

	@Override
	public void tuneListener() {
		coursesMain.getReportEditor().addEnableSettingListenerToCancelButton(this);
	}
}
