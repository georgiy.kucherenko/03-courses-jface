package com.luxoft.gui_jface;

import com.luxoft.courses_api.course_managers.FileCourseManager;
import com.luxoft.courses_api.service.SchoolService;
import com.luxoft.gui_jface.gui.CoursesMain;

/**
 * This class provides an entry point to Courses App. 
 * Start using Courses App by calling main method
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CoursesApp {

	/**
	 * Entry point method for Courses App
	 * 
	 * @param args - no need to provide args.
	 */
	public static void main(String[] args) {
		new CoursesMain(new SchoolService(new FileCourseManager())).run();
	}
}
