package com.luxoft.courses_api.course_managers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import com.luxoft.courses_api.exceptions.FileCourseManagerException;
import com.luxoft.courses_api.models.*;

class FileCourseManagerTest {
	private static final String PATH_STRING = "serialize_ckeck.tmp";
	private static final String ERROR_DELETING_FILE = "Error deleting temp file";
	@Mock
	private Course mockCourse;
	private FileCourseManager manager;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		manager = new FileCourseManager();
	}

	@AfterAll
	static void dispose() {
		try {
			Files.deleteIfExists(Path.of(PATH_STRING));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(ERROR_DELETING_FILE);
		}
	}

	@Test
	@DisplayName("When save entity and load it back then receive equal objects")
	void whenSaveEntityAndLoadItThenReceiveEqualObjects() {
		try {
			Course expectedCourse = new Course(new ArrayList<Report>(List.of(
					new Report("Vasya", "1", true), 
					new Report("Petya", "2", false)
					)));
			manager.saveCourse(PATH_STRING, expectedCourse);
			Course actualCourse = manager.loadCourse(PATH_STRING);
			assertEquals(expectedCourse, actualCourse);
		} catch (FileCourseManagerException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("When pass null as Path to saveCourse() then receive NPE")
	void whenPassNullAsPathToSaveCourseThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> manager.saveCourse(null, mockCourse));
	}
	
	@Test
	@DisplayName("When pass null as Path to loadCourse() then receive NPE")
	void whenPassNullAsPathToLoadCourseThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> manager.loadCourse(null));
	}
	
	@Test
	@DisplayName("When pass null as Course in saveCourse() then receive FileCourseManagerException")
	void setNullAsCourseWhenCallSaveMethod() {
		assertThrows(FileCourseManagerException.class, () -> manager.saveCourse(PATH_STRING, null));
	}

	@Test
	@DisplayName("When pass wrong file name then receive FileCourseManagerException")
	void passWrongFileThenReceiveException() {
		assertThrows(FileCourseManagerException.class, () -> manager.loadCourse("WRONG FILE NAME"));
		assertThrows(FileCourseManagerException.class, () -> manager.saveCourse("DD:\\!?>./", mockCourse));
	}
}
