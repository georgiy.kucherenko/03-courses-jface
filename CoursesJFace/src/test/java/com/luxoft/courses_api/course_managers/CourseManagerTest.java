package com.luxoft.courses_api.course_managers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import com.luxoft.courses_api.models.Course;


class CourseManagerTest {
	@Mock
	private CourseManager mockManager;  
	
	
	@BeforeEach 
	void init() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	@DisplayName("When call createCourse() then receive new Course object with initialized fields")
	void whenCallCreateCourseThenRecieveBlankObject() {
		when(mockManager.createCourse()).thenCallRealMethod();
		Course course = mockManager.createCourse();
		assertNotNull(course);
		assertNotNull(course.getReportList());
	}
}
