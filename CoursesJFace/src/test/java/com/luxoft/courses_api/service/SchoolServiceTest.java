package com.luxoft.courses_api.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import com.luxoft.courses_api.course_managers.CourseManager;
import com.luxoft.courses_api.exceptions.*;
import com.luxoft.courses_api.models.*;

class SchoolServiceTest {
	private final static String SOURCE = "URI / file name / smth else that is supported";
	@Mock
	private Course mockCourse;
	@Mock
	private CourseManager mockManager;
	private SchoolService service;


	@BeforeEach
	void setUp() throws FileCourseManagerException {
		MockitoAnnotations.openMocks(this);
		service = new SchoolService(mockManager);
	}

	@Test
	@DisplayName("When call createCourse() then mockManager.createCourse() is called")
	void testCreateCourse() {
		when(mockManager.createCourse()).thenReturn(mockCourse);
		Course actualCourse = service.createCourse();
		Mockito.verify(mockManager).createCourse();
		assertEquals(mockCourse, actualCourse);
	}

	@Test
	@DisplayName("When call loadCourse() then mockManager.loadCourse() is called")
	void testLoadCourse() {
		try {
			when(mockManager.loadCourse(SOURCE)).thenReturn(mockCourse);
			Course actualCourse = service.loadCourses(SOURCE);
			Mockito.verify(mockManager).loadCourse(SOURCE);
			assertEquals(mockCourse, actualCourse);
		} catch (CourseManagerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("When call saveCourse() then mockManager.saveCourse() is called")
	void testSaveCourse() {
		try {
			service.saveCourse(SOURCE, mockCourse);
			Mockito.verify(mockManager).saveCourse(SOURCE, mockCourse);
		} catch (CourseManagerException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("When pass null to loadCourse() then don't receive any exception")
	void whenPassNulltoLoadCourseServiceContinuesToWork() {
		try {
			service.loadCourses(null);
			verify(mockManager).loadCourse(null);
		} catch (CourseManagerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("When pass null to saveCourse() then don't receive any exception")
	void whenPassNulltoSaveCourseServiceContinuesToWork() {
		try {
			service.saveCourse(null, null);
			verify(mockManager).saveCourse(null, null);
		} catch (CourseManagerException e) {
			e.printStackTrace();
		}
	}
}
