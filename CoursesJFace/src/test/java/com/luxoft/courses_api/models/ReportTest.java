package com.luxoft.courses_api.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.mockito.*;


import nl.jqno.equalsverifier.EqualsVerifier;

class ReportTest {
	@Mock
	private Report mockReport;
	
	
	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("Testing getters/setters/equals")
	void testingGettersSetters() {
		Report actual = new Report("testName", "testGroup", false);
		
		assertNotEquals(actual, mockReport);
	
		actual.setName(mockReport.getName());
		actual.setGroup(mockReport.getGroup());
		actual.setDone(mockReport.getDone());
		
		assertEquals(actual, mockReport);
	}
	
	@Test
	@DisplayName("Testing equals with EqualsVerifier")
	public void equalsContract() {
	    EqualsVerifier.simple().forClass(Report.class).verify();
	}
	
}
