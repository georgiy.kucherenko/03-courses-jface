package com.luxoft.courses_api.models;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.*;

import org.junit.jupiter.api.*;
import org.mockito.*;


import nl.jqno.equalsverifier.EqualsVerifier;


class CourseTest {
	@Mock
	private Report mockReport;
	@Mock
	private List<Report> mockReportList;
	private Course course;
	

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		course = new Course(mockReportList);
	}

	@Test
	@DisplayName("When pass null instead of reportList to constructor then receive NPE")
	void whenPassNullToConstructorThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> new Course(null).addReport(mockReport));
	}
	
	@Test
	@DisplayName("When set null instead of reportList then receive NPE")
	void whenSetNullInsteadOfReportListThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> {
			course.setReportList(null);
			course.addReport(mockReport);
		});
	}

	@Test
	@DisplayName("When call addReport() then mockReportList.add() is called")
	void whenCallAddReportThanReportListAddIsCalled() {
		course.addReport(mockReport);
		verify(mockReportList).add(mockReport);
	}
	
	@Test
	@DisplayName("When call removeReport() then mockReportList.remove() is called")
	void whenRemoveReportThenReportListModifiedSuccessfully() {
		course.removeReport(mockReport);
		verify(mockReportList).remove(mockReport);
	}
	
	@Test
	@DisplayName("When call setReport() then mockReportList.set() is called")
	void whenSetReportToListThenReportListModifiedSuccessfully() {
		final int INDEX = 0;
		course.setReport(INDEX, mockReport);
		verify(mockReportList).set(INDEX, mockReport);
	}
	
	@Test
	@DisplayName("Testing equals with EqualsVerifier")
	public void equalsContract() {
	    EqualsVerifier.simple().forClass(Course.class).verify();
	}

	@Test
	@DisplayName("Testing getters/setters/equals")
	void whenCallGettersSettersAndEqualsThenAllIsOk() {
		course.setReportList(new ArrayList<Report>(List.of(new Report("John", "1", true))));
		
		Course mockCourse = mock(Course.class);
		assertNotEquals(mockCourse, course);
		
		course.setReportList(mockCourse.getReportList());
		assertEquals(mockCourse.getReportList(), course.getReportList());
	}
}
